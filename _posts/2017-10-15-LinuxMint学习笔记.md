---
layout: default
title:  "LinuxMint学习笔记"
date:   2017-10-15 19:06:00 +0800

---

# Linux Mint学习笔记

1. 缺少C++ compiler等基本的组件
	使用`$sudo apt-get install build-essential`
2. 可以用gcc把.c转成.s文件`$gcc -S FileName.c`
3. git的基本操作
```
$git init 		#初始化仓库，建立.git文件夹，记录修改
$git clone url.git 	#获取远程文件夹
$git commit -m 'Statement for the modifying'  	#提交一次修改并注释修改，时刻做标记是个好习惯
$git push -u origin master	#提交到github仓库
$git rm fileName1 fileNmae2...  #删除文件
$git rmdir directory #删除路径
```
4. sublime中文输入问题
```
$git clone https://github.com/lyfeyaj/sublime-text-imfix.git
$cd ~/sublime-text-imfix
$sudo cp ./lib/libsublime-imfix.so /opt/sublime_text/
$sudo cp ./src/subl /usr/bin/
此时可以在终端里面使用subl来打开sublime，把面板的sublime启动命令修改为subl即可，至于右键可以在其他方式里面加入subl选项，以后打开文件使用此选项即可
```
5. Atom的命令行安装
```
$sudo add-apt-repository ppa:webupd8team/atom
$sudo apt-get update
$sudo apt-get install atom
```
6. 查看磁盘占用情况
`$du -sh 路径`
7. 显示系统环境变量的几种命令
```
env显示当前用户的环境变量，不会显示自定义的环境变量
export功能和env一样，但是输出按变量名排序
declare显示当前shell所有变量，包括系统和自定义，输出的按变量名进行排序
set同declare
即env和export现实的环境变量
set和declare显示的是环境变量和自定义变量
```
8. 设置环境变量需要注意的一个问题
`$ export PATH=$PATH:/home/to/operation_tools`
这里的$PATH一定不能省略，否则会覆盖原有的PATH

9. 安装shadowsocks的命令行版本
```
sudo apt-get install python-pip
sudo pip install shadowsocks
```
出现了一个问题，
>Could not import setuptools which is required to install from a source distribution.Please install setuptools.

	需要安装setuptools工具，使用`sudo pip install -U setuptools`，其中`-U`用于升级,使用pip install -help查看帮助,相应的选项如下

	>-U, --upgrade               Upgrade all specified packages to the newest
                              available version. The handling of dependencies
                              depends on the upgrade-strategy used.
	-U,	--upgrade 							把所有相关包升级到最新的可用版本，依赖关系的处理取决于所使用的更新策略

	创建配置文件`sudo vi /etc/shadowsocks.json`

	```
{
"server":"server-ip",
"server_port":8000,
"local_address": "127.0.0.1",
"local_port":1080,
"password":"your-password",
"timeout":600,
"method":"aes-256-cfb"
}
```
保存之后可以终端打开了
`sslocal -c /etc/shadowsocks.json`
后台运行ss
`sudo sslocal -c /etc/shadowsocks.json -d start`
设置开机启动
 编辑文件/etc/rc.local,在exit 0之前加入一行`sudo sslocal -c /etc/shadowsocks.json -d start`
 查看ss是否启动
 `sudo systemctl status rc-local.service`
 如果启动显示如下
 >rc-local.service - /etc/rc.local Compatibility
Loaded: loaded (/etc/systemd/system/rc-local.service; enabled; vendor preset: enabled)
Active: active (running) since Fri 2015-11-27 03:19:25 CST; 2min 39s ago
Process: 881 ExecStart=/etc/rc.local start (code=exited, status=0/SUCCESS)
CGroup: /system.slice/rc-local.service
├─ 887 watch -n 60 su matrix -c ibam
└─1112 /usr/bin/python /usr/local/bin/sslocal -c /etc/shadowsocks....

10. mysql的安装
```
sudo apt-get install mysql-server#此处tab一下可选版本
sudo mysql_secure_installation#设置密码
sudo mysql_install_db
```
11. 文件权限和文件所属用户/组
```
chown -hR group.user dir		
chown root /u		将 /u 的属主更改为"root"。
chown root:staff /u	和上面类似，但同时也将其属组更改为"staff"。
chown -hR root /u	将 /u 及其子目录下所有文件的属主更改为"root"。
```

12. Linux下使用apt安装mysql的主要的文件分布
/var/lib/mysql该路径的属组属主默认均为mysql,用于存放mysql的数据库文件和一些配置文件
13. tmux终端分屏工具
安装`sudo apt-get install tmux`
	1. `tmux new -s kentlinux`启动一个全新的tmux会话,名称是kentlinux,-s是session的缩写
	2. 新建一个窗口 先按一下ctrl＋B组合按键,然后松开按一下C按键
	3. 窗口之间切换 先按一下ctrl＋B组合按键,然后松开按一下相应的bash数字键
	4. `watch -n 2 free`每隔两秒更新一次内存的使用状况
	5. 使用按一下ctrl＋B再次按一下d按键,可以把当前的tmux会话保留,之后可以再次回到当前回话,这对于服务器的操作很有意义
	6. 之后可以使用`tmux ls`查看所有的会话
	7. 使用`tmux a -t 会话名`可以回到之前的会话
	8. 假如正在操作远程的服务器,突然有事离开,想关闭电脑,又不想停下远程服务器的工作,可以这样使用,的确是一个很有用处的工具
14. alias别名,方便备注记忆命令.但是这样设置的别名只在当前的bash中有效.退出bash后立即失效
`alias [name[=value]]`注意在=的前后不能有空格,否则出错.bash语句里面同样有这个问题
	1. 查看已经设置的所有别名`alias`
	2. 单独查看已经设置的某一个别名`alias 别名命令`
	3. 取消别名`unalias 别名命令`
	4. 如果别名与实际的命令冲突可以使用以下三种方法解决
		1. 使用命令的绝对路径
		2. 进入命令所在目录使用./command
		3. *在命令前面使用反斜线\*
	5. 可以把别名设置加入.alias文件里面来永久保存
15. 叹号定位
	1. 输入	`history`查看历史命令
	使用`!编号`执行某个编号的历史命令
	2. `!!`可以执行上次的命令
	3. 使用`!-1`可以执行上次的命令
	4. 使用ctrl＋p可以执行上次的命令
16. 历史命令的相关操作,使用ctrl＋R搜索历史命令,左右方向键可以微调.使用`hostory -c`可清除所有的历史命令
17. `\time`可以查看详细的时间使用信息.注意:shell内置的time没有/usr/bin/time的强大
18. 强大的find命令
	1. `find [path...] -name [pattern]`
	2. 简单的匹配实例
		1. `find / -name "*.txt"`
		2. `find / -name "test.md"`
		3. `find . -type d -name "test*"`

			linux世界皆文件
			- 普通文件
			- 目录
			- 符号链接文件
			- 管道文件
			- 设备文件

			-type支持的类型很多
			- d:文件夹
			- f:普通文件
			- l:链接文件
			- b:块设备
			- c:字符设备
			- p:管道文件
			- s:socket套接字
	3. 使用正则表达式
	如使用正则表达式匹配一个以e开头含有avi字符串并以数字结尾的普通文件

	`find / -type f -regex '.*/e.*avi.*[0-9]+$'`
	4. 按照属组属主搜索.
	如查找当前目录属主为kent和属组为kent的普通文件有哪些`find . -type f -user kent -group kent `
	5. 按照权限来搜索.
	如搜索权限为664(rw-rw-r--)的文件`find . -perm 664`这里perm是permission的缩写
	6. *很强大的命令在此*
	搜索之后可以使用`-exec`来把搜索到的对象作为shell命令的执行对象
	如`find . -perm 664 -type f -exec mv {} {}.new \; `语义分析:查找当前目录下权限为664的普通文件,并加上后缀名.new.{}表示查找到的对象,分号在-exec的用法中表示命令结束,加上反斜杠用于防止转义.
	7. 按照时间搜索
		- -mmin+n表示n分钟以前文件被修改过
		- -mmin-n表示n分钟以内文件被修改过

		- -cmin+n表示n分钟以前文件状态有过改变
		- -cmin-n表示n分钟以内文件状态有过改变

		- -amin+n表示n分钟以前文件被访问过
		- -amin-n表示n分钟以内文件被访问过
		把min换成time就可以使用天为单位了
	8. 按照大小查找
	*这个方法很重要*
	如`find . -type f -size +40M -exec ls -hl {} \;`
		查看当前目录下大小大于40M的所有文件

		- -size +40M搜索大于40M的文件
		- -size -40M...小于........
		- -size  40M...等于........
		> -size支持的单位有b(512byte),c(bytes),w(双字节的字),k(注意是小写,表示KB),M,G

	9. find默认是递归搜索的.如果不想递归搜索,可以使用`-maxdepth n`,如果n=1, 就是在目录的第一级路径搜索
19. ssh(secure shell安全外壳协议)秘钥
	1. 进入.ssh目录`cd ~/.ssh`
	2. 查看所有`ls`
	生成秘钥
	3. `ssh-keygen -t rsa -C "emailAddress"`	其中-t 指定的秘钥的类型,默认的是rsa,-C设置注释文字,比如邮箱
